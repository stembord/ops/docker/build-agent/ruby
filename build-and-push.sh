#!/bin/bash

repo="stembord/build-agent-ruby"

function build_and_push() {
  local version=$1
  docker build --build-arg RUBY_VERSION=${version} -t ${repo}:${version} .
  docker push ${repo}:${version}
}

build_and_push "2.5"
build_and_push "2.6"